package mblog.web.controller.minigram;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sun.org.apache.xpath.internal.operations.Bool;
import mblog.base.context.AppContext;
import mblog.base.upload.FileRepo;
import mblog.base.utils.FilePathUtils;
import mblog.base.utils.ImageUtils;
import mblog.modules.blog.data.FavorVO;
import mblog.modules.blog.data.PostVO;
import mblog.modules.blog.data.SignFavorVO;
import mblog.modules.blog.data.SignVO;
import mblog.modules.blog.entity.*;
import mblog.modules.blog.service.FavorService;
import mblog.modules.blog.service.PostService;
import mblog.modules.blog.service.SignFavorService;
import mblog.modules.blog.service.SignService;
import mblog.modules.user.data.OpenOauthVO;
import mblog.modules.user.data.UserVO;
import mblog.modules.user.entity.OpenOauth;
import mblog.modules.user.service.OpenOauthService;
import mblog.modules.user.service.UserService;
import mblog.modules.utils.AliOSSUtil;
import mblog.modules.utils.http.HttpAPIService;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.net.www.http.HttpClient;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

@Controller("minigramController")
@RequestMapping("/minigram")

public class MinigramController {
    @Autowired
    private PostService postService;
    @Autowired
    private SignService signService;

    @Autowired
    private FavorService favorService;

    @Autowired
    private OpenOauthService openOauthServer;

    @Autowired
    private UserService userService;

    @Autowired
    private SignFavorService signFavorService;

    @Resource
    private HttpAPIService httpAPIService;

    @Autowired
    private AppContext appContext;

    @Autowired
    protected FileRepo fileRepo;

//文章列表
    @RequestMapping("/list")
    @ResponseBody
    public List<PostVO> channelList(Integer channelId, Integer page, Integer size) {
        Pageable pageable = new PageRequest(page - 1, size);
        Page<PostVO> result = postService.paging(pageable, channelId, null, "newest");
        List<PostVO>  list = result.getContent();
        for (int index = 0; index<list.size();index++){
            PostVO item = list.get(index);
            item.setSignCount(signService.countAllSignByPostId(item.getId()));
        }

        return list;
    }

    //用户查看自己收藏的文学之声文章列表
    @RequestMapping("/userPostList")
    @ResponseBody
    public Page<FavorVO> userList(Integer userId, Integer page, Integer size) {
        Pageable pageable = new PageRequest(page - 1, size);
        Page<FavorVO> list = favorService.pagingByOwnId(pageable,userId);
        return list;
    }

    //用户添加收藏指定的文学之声文章
    @RequestMapping("/userAddCollect")
    @ResponseBody
    public Favor addFavorRecord(long userId, long postId) {
        Favor favor = favorService.addFavorRecord(userId,postId);
        return favor;
    }

    //用户添加收藏指定的文学之声文章
    @RequestMapping("/userCollectExist")
    @ResponseBody
    public Favor judgeFavor(long userId, long postId) {
        Favor favor = favorService.judgeFavor(userId,postId);
        return favor;
    }


//文章详情
    @RequestMapping("/detail")
    @ResponseBody
    public PostVO channelList(long postId) {
        PostVO result = postService.get(postId);
        return result;
    }

//    首页显示
    @RequestMapping("/newArticle")
    @ResponseBody
    public PostVO newArticle() {
        PostVO result = postService.getOne();
        return result;
    }

    //首页显示
    @RequestMapping("/signByPostId")
    @ResponseBody
    public List<SignVO> allSign(long postId,int pageIndex,int pageSize) {
        List<SignVO> result = signService.findAllSign(postId,pageIndex,pageSize);
        return result;
    }


    //打卡列表
    @RequestMapping("/allSigns")
    @ResponseBody
    public List<SignVO> allSigns(int pageIndex,int pageSize) {
        List<SignVO> result = signService.findAllSigns(pageIndex,pageSize);
        return result;
    }
    //某用户打卡记录

    //打卡列表
    @RequestMapping("/signByUserId")
    @ResponseBody
    public List<SignVO> findAllSignByUserId(long userId, int pageSize, int pageIndex) {
        List<SignVO> result = signService.findAllSignByUserId(userId,pageIndex,pageSize);
        return result;
    }

    //音频上传
    @PostMapping("/uploadVoice")
    @ResponseBody
    public String uploadExcel(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
        String fileUrl = "";
        try {

//	        log.info("filename:{}", file.getOriginalFilename());
            if (file != null) {
//                        FileModel fileItem = new FileModel();
                // 取得当前上传文件的文件名称
                String fileName = file.getOriginalFilename();
//                        fileItem.name = fileName;
//                        fileItem.type = fileName.substring(fileName.lastIndexOf(".") + 1);
                // 如果名称不为空,说明该文件存在，否则说明该文件不存在
                if (fileName.trim() != "") {
                    File newFile = new File(fileName);
                    FileOutputStream outStream = new FileOutputStream(newFile); // 文件输出流用于将数据写入文件
                    outStream.write(file.getBytes());
                    outStream.close(); // 关闭文件输出流
                    file.transferTo(newFile);
                     fileUrl = AliOSSUtil.upload(newFile);
                    // 上传到阿里云
//                            fileList.add(fileItem);
                    newFile.delete();

                    //去除前后双引号
//                    fileUrl = fileUrl.substring(1, fileUrl.length()-1);
                    return fileUrl;
                }
                return fileUrl;
            }
        } catch (Exception e) {
//	    log.error("upload>>>>异常:{}", e.toString());
            return fileUrl;
        }
        return fileUrl;
    }

    //打卡列表
    @RequestMapping("/signAdd")
    @ResponseBody
    public Sign findAllSignByUserId(long userId, long postId, String audioUrl, String comment,int length) {
        audioUrl = audioUrl.replace("\"","");
        return signService.saveSign(userId,postId,audioUrl,comment,length);
    }


//    更新用户登录信息
    @RequestMapping("/updateUserNameAndImage")
    @ResponseBody
    public UserVO updateUserNameAndImage(long userId, String nickName, String avatarUrl) throws Exception {

        UserVO u = userService.get(userId);
        // ===将远程图片下载到本地===
        String ava100 = appContext.getAvaDir() + getAvaPath(u.getId(), 100);
        ImageUtils.download(avatarUrl, fileRepo.getRoot() + ava100);
        userService.updateAvatar(u.getId(), ava100);
        u.setName(nickName);
        userService.update(u);
        return userService.get(userId);
    }
    public String getAvaPath(long uid, int size) {
        String base = FilePathUtils.getAvatar(uid);
        return String.format("/%s_%d.jpg", base, size);
    }

    //静默登录
    @RequestMapping("/loginCode")
    @ResponseBody
    public UserVO loginCode(String code) throws Exception {

        String url = "https://api.weixin.qq.com/sns/jscode2session?appid=wx18dc809fc3b81751&secret=ea0d383ddde6d4a3544a76032c731724&js_code=" + code + "&grant_type=authorization_code";
        String str = httpAPIService.doGet(url);
        System.out.println(str);
        JSONObject object = JSON.parseObject(str);
        String openid = object.getString("openid");
        String session_key = object.getString("session_key");

        OpenOauthVO oauthVO = openOauthServer.getOauthByOauthUserId(openid);

        if (oauthVO == null){
            UserVO userVo = new UserVO();

            userVo.setUsername("id" + System.currentTimeMillis());
            userVo.setName("id" + System.currentTimeMillis());
            userVo.setPassword("123456");

            userVo = userService.register(userVo);

            //创建第三方登录认证
            OpenOauthVO oauthV = new OpenOauthVO();
            oauthV.setUserId(userVo.getId());
            oauthV.setAccessToken(session_key);
            oauthV.setOauthUserId(openid);
            openOauthServer.saveOauthToken(oauthV);
            return userService.get(userVo.getId());
        }else{
            return  userService.get(oauthVO.getUserId());
        }
    }

    //静默登录
    @RequestMapping("/praiseSign")
    @ResponseBody
    public SignFavor praiseSign(long signId,long userId) throws Exception {
        SignFavor signFavor = signFavorService.saveSignFavor(userId,signId);
       return signFavor;
    }



}
