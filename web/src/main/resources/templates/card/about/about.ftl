<#include "/default/utils/ui.ftl"/>

<@layout "关于我们">

<div class="row main">
    <div class="col-xs-12 col-md-9 side-left topics-show">
        <!-- view show -->
        <div class="topic panel panel-default">
            <div class="infos panel-heading">

                <h1 class="panel-title topic-title">关于我们</h1>

                <div class="meta inline-block">
                    <a class="author" href="#">官方团队</a>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="content-body entry-content panel-body ">
                <div class="markdown-body" id="emojify">
                    <p><strong>关于演讲宝</strong><br/></p>
                    <p>演讲宝由一群热爱演讲懂互联网的人创建，致力于为广大演讲爱好者提供一个优质丰富的演讲共享平台，演讲宝以互联网技术为基础，诚邀广大演讲爱好者分享自己优秀的演讲稿件和关于演讲的真知灼见，一起学习共同进步。</p>
                    <p><strong>网站初衷</strong><br/></p>
                    <p>在这里，我们将会分享各类演讲素材，演讲使用的技巧，以及演讲的心得，帮助新人快速进步提高演讲水平，让无法下笔的你找到写作的灵感，让演讲高手互相切磋，共同进步！
                    </p>
                    <!--
                    <p><strong>技术架构</strong><br/></p>
                    <p>mtons采用通用的SSH框架构建</p>
                    <p> 页面端采用Velocity模板语言</p>
                    <p>存储采用Ehcache、MySQL，搜索引擎使用的Lucene</p>
                    <p>前端为响应式设计，主要使用Bootstrap框架+jQuery。</p>
                    -->
                    <p><strong>网站栏目介绍</strong></p>
                    <p>涵盖内容范围：比赛演讲稿 竞聘演讲稿 英语演讲稿 发言稿 自我介绍 辩论词 征文演讲稿 演讲技巧
                        经验介绍 心得体会  转正演讲稿 就职演讲稿 汇报材料 调研报告 工作报告 新闻报道
                        诗歌散文 情书 企业家论坛演讲 客户答谢会演讲 产品发布会演讲 上市路演演讲
                        员工大会演讲 社会公益演讲
                        庆典演讲 新年致辞 书籍小说等</p>
                    <p>演讲技巧：由网站合约达人为大家定期分享演讲经验</p>
                    <p>文学之声：定期发布诗歌、电影对白、经典语录等文学经典</p>
                    <p>演讲活动：目前只发布成都最新的各类演讲消息及活动</p>
                    <p>演讲达人：平台签约演讲达人及相关演讲组织介绍</p>

                </div>
            </div>
            <div class="panel-footer operate">
            </div>
            <div class="panel-footer operate">
                <div class="hidden-xs">
                    <div class="social-share" data-sites="weibo, wechat, facebook, twitter, google, qzone, qq"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- /view show -->
    </div>
    <div class="col-xs-12 col-md-3 side-right hidden-xs hidden-sm">
		<#include "/default/inc/right.ftl"/>
    </div>
</div>

</@layout>