<#include "/default/utils/ui.ftl"/>

<@layout "加入我们">

<div class="row main">
    <div class="col-xs-12 col-md-9 side-left topics-show">
        <!-- view show -->
        <div class="topic panel panel-default">
            <div class="infos panel-heading">

                <h1 class="panel-title topic-title">加入我们</h1>

                <div class="meta inline-block">
                    <a class="author" href="#">官方团队</a>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="content-body entry-content panel-body ">
                <div class="markdown-body" id="emojify">
                    <p><strong>联系我们</strong></p>
                    <p>如果你对演讲一窍不通，但是工作中需要发言，欢迎联系我们！</p>
                    <p> 如果你想提高英语水平，想找到一个英语交流练习的平台，欢迎联系我们！</p>
                    <p> 如果你想让自己的演讲水平更进一步，驰骋讲台，欢迎联系我们！</p>
                    <p><strong>加入我们</strong><br/></p>
                    <p>如果你喜欢演讲、手里有不少好的作品资源想和我们一起分享，欢迎加入我们，如果你是演讲教练、培训师可以提供专业的演讲和领导力方面的培训，欢迎加入我们!</p>
                    <p>QQ：2138886073</p>
                    <p>邮箱：guansichou@126.com</p>
				</div>
            </div>
            <div class="panel-footer operate">
            </div>
            <div class="panel-footer operate">
                <div class="hidden-xs">
                    <div class="social-share" data-sites="weibo, wechat, facebook, twitter, google, qzone, qq"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- /view show -->
    </div>
    <div class="col-xs-12 col-md-3 side-right hidden-xs hidden-sm">
		<#include "/default/inc/right.ftl"/>
    </div>
</div>

</@layout>