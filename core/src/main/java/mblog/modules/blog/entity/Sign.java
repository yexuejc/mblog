package mblog.modules.blog.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.hibernate.search.annotations.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.annotations.SortableField;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 内容表
 *
 * @author langhsu
 */
@Entity
@Table(name = "sign")
@Indexed(index = "sign")
@Analyzer(impl = SmartChineseAnalyzer.class)
@Data
public class Sign implements Serializable {
    private static final long serialVersionUID = 7144425803920583423L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SortableField
    @NumericField
    private long id;



    /**
     * 用户Id
     */
    @Field
    @NumericField
    @Column(name = "user_id")
    private long userId;

    /**
     * 评论
     */
    @Field
    private String comment;

    /**
     * 音频
     */
    private String audio;


    @Field
    @NumericField
    @Column(name = "length")
    private int length; // 作者

    @Field
    @JSONField(format="yyyy-MM-dd")
    @Column(name = "created")
    private Date created;


    /**
     * 用户Id
     */
    @Field
    @NumericField
    @Column(name = "post_id")
    private long postId;

    /**
     * 置顶状态
     */
    private int weight;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

//    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public int getWeight() {
        return weight;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}