package mblog.modules.blog.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.hibernate.search.annotations.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "sign_favor")

@Indexed(index = "sign_favor")
@Analyzer(impl = SmartChineseAnalyzer.class)
@Data
public class SignFavor implements Serializable {
    private static final long serialVersionUID = 7144421111120583423L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SortableField
    @NumericField
    private long id;



    /**
     * 用户Id
     */
    @Field
    @NumericField
    @Column(name = "user_id")
    private long userId;


    /**
     * 打卡Id
     */
    @Field
    @NumericField
    @Column(name = "sign_id")
    private long signId;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getSignId() {
        return signId;
    }

    public void setSignId(long signId) {
        this.signId = signId;
    }
}
