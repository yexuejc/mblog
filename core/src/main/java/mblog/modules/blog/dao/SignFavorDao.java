package mblog.modules.blog.dao;


import mblog.modules.blog.entity.Sign;
import mblog.modules.blog.entity.SignFavor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface SignFavorDao extends JpaRepository<SignFavor, Long>, JpaSpecificationExecutor<SignFavor> {

    /*找到所有点赞的用户*/
    List<SignFavor> findAllBySignId(long signId);

    SignFavor findByUserIdAndSignId(long userId, long signId);

    SignFavor save(SignFavor signFavor);

}
