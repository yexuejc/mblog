package mblog.modules.blog.dao;

import mblog.modules.blog.entity.Favor;
import mblog.modules.blog.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author langhsu on 2015/8/31.
 */
public interface FavorDao extends JpaRepository<Favor, Long>, JpaSpecificationExecutor<Favor> {
    /**
     * 指定查询
     * @param ownId
     * @param postId
     * @return
     */
    Favor findByOwnIdAndPostId(long ownId, long postId);

    Page<Favor> findAllByOwnIdOrderByCreatedDesc(Pageable pageable, long ownId);

    Favor save(Favor favor);


}
