package mblog.modules.blog.dao;

import mblog.modules.blog.entity.Sign;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SignDao extends JpaRepository<Sign, Long>, JpaSpecificationExecutor<Sign> {


    Page<Sign> findByPostId(Pageable pageable,Long postId);

    Page<Sign> findAll(Pageable pageable);

    long countByPostId(long postId);

    Page<Sign> findByUserId(Pageable pageable,Long userId);

    Sign save(Sign sign);

    long countByUserId(long userId);
}

