package mblog.modules.blog.service.impl;
import com.sun.org.apache.xpath.internal.operations.Bool;
import mblog.base.context.SpringContextHolder;
import mblog.base.lang.Consts;
import mblog.base.lang.EntityStatus;
import mblog.base.utils.PreviewTextUtils;
import mblog.core.event.PostUpdateEvent;
import mblog.modules.blog.dao.SignDao;
import mblog.modules.blog.dao.SignFavorDao;
import mblog.modules.blog.data.PostVO;
import mblog.modules.blog.data.SignFavorVO;
import mblog.modules.blog.data.SignVO;
import mblog.modules.blog.entity.*;
import mblog.modules.blog.service.SignService;
import mblog.modules.user.data.UserVO;
import mblog.modules.blog.dao.PostAttributeDao;
import mblog.modules.blog.dao.PostDao;
import mblog.modules.user.entity.User;
import mblog.modules.utils.BeanMapUtils;
import mblog.modules.blog.service.ChannelService;
import mblog.modules.blog.service.FavorService;
import mblog.modules.blog.service.PostService;
import mblog.modules.user.service.UserEventService;
import mblog.modules.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import java.util.*;


@Service
@Transactional

public class SignServiceImpl implements SignService {

    @Autowired
    private PostDao postDao;
    @Autowired
    private PostService postService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserEventService userEventService;
    @Autowired
    private FavorService favorService;
    @Autowired
    private ChannelService channelService;
    @Autowired
    private PostAttributeDao postAttributeDao;
    @Autowired
    private SignDao signDao;
    @Autowired
    private SignFavorDao signFavorDao;



    //查看指定文章所有的打卡记录
    @Override
    @Transactional
    public List<SignVO> findAllSign(long postId, int pageNumber,int pageSize) {
        //service
        Sort sort = new Sort(Sort.Direction.DESC,"created"); //创建时间降序排序
        Pageable pageable = new PageRequest(pageNumber - 1,pageSize,sort);
        Page<Sign> list = signDao.findByPostId(pageable,postId);
        List<SignVO> volist = new ArrayList<SignVO>();
        for (Sign po : list.getContent()) {
            SignVO d = null;
            if (po != null) {
                d = BeanMapUtils.copy(po, 1);
//                d.setCreated(po.getCreated());
                d.setUser(userService.get(d.getUserId()));
//                d.setPost(postService.get(d.getPostId()));
                d.setPraiseUsers(this.findSignPraiseUsers(d.getId()));
            }
            volist.add(d);
        }
        return volist;
    }

//查看所有打卡记录
    @Transactional
    public List<SignVO>findAllSigns(int pageNumber,int pageSize){
//        Pageable pageable = new PageRequest(pageNumber,pageSize);
        Sort sort = new Sort(Sort.Direction.DESC, "created");
        Pageable pageable = new PageRequest(pageNumber - 1, pageSize, sort);
        Page<Sign> list = signDao.findAll(pageable);

        List<SignVO> volist = new ArrayList<SignVO>();
        for (Sign po : list.getContent()) {
            SignVO d = null;
            if (po != null) {
                d = BeanMapUtils.copy(po, 1);
//                d.setCreated(po.getCreated());
                d.setUser(userService.get(d.getUserId()));
//                d.setPost(postService.get(d.getPostId()));
                d.setPraiseUsers(this.findSignPraiseUsers(d.getId()));
            }
            volist.add(d);
        }
        return volist;
    }

    //查看某打卡记录对应的点赞数
    @Transactional
    public List<UserVO>findSignPraiseUsers(long signId){
        List<SignFavor> list = signFavorDao.findAllBySignId(signId);
        List<UserVO> volist = new ArrayList<UserVO>();
        for (int item = 0; item < list.size();item++) {
            SignFavor po = list.get(item);
            SignFavorVO d = new SignFavorVO();
            d = BeanMapUtils.copy(po, 1);
            UserVO userVo = userService.get(d.getUserId());
            volist.add(userVo);
        }
        return volist;
    }

   public long countAllSignByPostId(long postId){
        return signDao.countByPostId(postId);
    }

    public List<SignVO> findAllSignByUserId(long userId, int pageNumber, int pageSize){
        Sort sort = new Sort(Sort.Direction.DESC, "created");
        Pageable pageable = new PageRequest(pageNumber - 1, pageSize, sort);

        Page<Sign> list = signDao.findByUserId(pageable,userId);

        List<SignVO> volist = new ArrayList<SignVO>();
        for (Sign po : list.getContent()) {
            SignVO d = null;
            if (po != null) {
                d = BeanMapUtils.copy(po, 1);
                d.setUser(userService.get(d.getUserId()));
                d.setPost(postService.get(d.getPostId()));
                d.setPraiseUsers(this.findSignPraiseUsers(d.getId()));
            }
            volist.add(d);
        }
        return volist;
    }

    public Sign saveSign(Long userId, long postId, String audioUrl, String comment, int length){
        Sign sign1 = new Sign();
        sign1.setAudio(audioUrl);
        sign1.setUserId(userId);
        sign1.setPostId(postId);
        sign1.setComment(comment);
        sign1.setLength(length);
        Date date=new Date();
        sign1.setCreated(date);
        Sign sign2 = signDao.save(sign1);
        return sign2;
    }
}
