package mblog.modules.blog.service.impl;

import mblog.modules.blog.dao.SignFavorDao;
import mblog.modules.blog.data.SignVO;
import mblog.modules.blog.entity.Sign;
import mblog.modules.blog.entity.SignFavor;
import mblog.modules.blog.service.SignFavorService;
import mblog.modules.blog.service.SignService;
import mblog.modules.utils.BeanMapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional

public class SignFavorServiceImpl implements SignFavorService {

    @Autowired
    private SignFavorDao signFavorDao;

    //查看指定用户是否已经对打卡点赞

    @Override
    @Transactional
    public SignFavor findBySignIdAndUserId(long signId, int userId) {
        SignFavor signFavor = signFavorDao.findByUserIdAndSignId(userId,signId);
        return signFavor;
    }

    @Override
    @Transactional
    public SignFavor saveSignFavor(long userId, long signId){
        SignFavor signFavor = signFavorDao.findByUserIdAndSignId(userId,signId);
        if (signFavor == null){
            signFavor = new SignFavor();
            signFavor.setSignId(signId);
            signFavor.setUserId(userId);
            return signFavorDao.save(signFavor);
        }else{
            return signFavor;
        }

    }
}
