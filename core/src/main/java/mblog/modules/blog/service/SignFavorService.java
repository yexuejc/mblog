package mblog.modules.blog.service;

import mblog.modules.blog.entity.Sign;
import mblog.modules.blog.entity.SignFavor;

public interface SignFavorService {

    SignFavor findBySignIdAndUserId(long signId, int userId);

    SignFavor saveSignFavor(long userId, long signId);

}
