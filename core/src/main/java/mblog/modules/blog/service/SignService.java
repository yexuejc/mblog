package mblog.modules.blog.service;

import com.sun.org.apache.xpath.internal.operations.Bool;
import mblog.modules.blog.data.PostVO;
import mblog.modules.blog.data.SignVO;
import mblog.modules.blog.entity.Sign;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface SignService {

//    Page<SignVO> findAllSign(int postId, int pageNumber, int pageSize);
    List<SignVO> findAllSign(long postId, int pageNumber,int pageSize);
    List<SignVO>findAllSigns(int pageNumber,int pageSize);

    long countAllSignByPostId(long postId);

    List<SignVO> findAllSignByUserId(long userId, int pageNumber, int pageSize);

    Sign saveSign(Long userId, long postId, String audioUrl, String comment, int length);


}
