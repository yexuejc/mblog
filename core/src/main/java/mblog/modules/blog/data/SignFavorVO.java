package mblog.modules.blog.data;

import com.alibaba.fastjson.annotation.JSONField;
import mblog.modules.user.data.UserVO;

import java.io.Serializable;
import java.util.Date;

public class SignFavorVO  implements Serializable {

    private static final long serialVersionUID = -1144627551517707138L;
    private long id;
    private long userId;
    private long signId;
    private Date created;
    private UserVO user;

    @JSONField(format="yyyy-MM-dd")
    public void setCreated(Date createTime) {
        this.created = created;
    }
    @JSONField(format="yyyy-MM-dd")
    public Date getCreated() {
        return created;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getSignId() {
        return signId;
    }

    public void setSignId(long signId) {
        this.signId = signId;
    }

    public UserVO getUser() {
        return user;
    }

    public void setUser(UserVO user) {
        this.user = user;
    }
}
