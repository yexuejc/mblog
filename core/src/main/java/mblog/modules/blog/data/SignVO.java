package mblog.modules.blog.data;

import com.alibaba.fastjson.annotation.JSONField;
import mblog.base.lang.Consts;
import mblog.modules.blog.entity.Channel;
import mblog.modules.blog.entity.Post;
import mblog.modules.blog.entity.PostAttribute;
import mblog.modules.user.data.UserVO;
import mblog.modules.user.entity.User;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/

/**
 * @author langhsu
 *
 */
public class SignVO implements Serializable {

    private long id;
    private String comment;
    private String audio;
    private int length;
    private Date created;
    private int weigh;

    private long userId;
    private long postId;

    private PostVO post;
    private UserVO user;

    //打卡点赞用户列表
    private List<UserVO> praiseUsers;

    private static final long serialVersionUID = -1144627551517707138L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }





    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    public Date getCreated() {
        return created;
    }


    public void setCreated(Date created) {
        this.created = created;
    }

    public int getWeigh() {
        return weigh;
    }

    public void setWeigh(int weigh) {
        this.weigh = weigh;
    }


    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public PostVO getPost() {
        return post;
    }

    public void setPost(PostVO post) {
        this.post = post;
    }

    public UserVO getUser() {
        return user;
    }

    public void setUser(UserVO user) {
        this.user = user;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<UserVO> getPraiseUsers() {
        return praiseUsers;
    }

    public void setPraiseUsers(List<UserVO> praiseUsers) {
        this.praiseUsers = praiseUsers;
    }
}
